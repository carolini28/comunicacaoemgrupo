/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemasdistribuidos;

import java.io.IOException;
import java.net.Socket;
/**
 *
 * @author Carolini
 */
public class ThreadRecebeMsg extends Thread{
    
    private Cliente cliente;
    private MulticastGroup multicastgroup;
    private ThreadEnviaMsgExterna enviaExterna = null;
    private ThreadRecebeMsgExterna recebeExt = null;
    private boolean perdeuAntes;
    
    ThreadRecebeMsg(Cliente cliente, MulticastGroup multicastgroup){
        this.cliente = cliente;
        this.multicastgroup = multicastgroup;
        perdeuAntes = false;
    }
    
    public void run() {
        
        try{
            boolean verificouTdsP = false;
            System.out.println("Recebe mensagens internas.");
            
            while(true){
                boolean verificouMsg = false;
                String msg =  multicastgroup.receiveMessage();
                String mostrarMsg;
                String mensagem[] = msg.split(" ");
                
                //verifica qual tipo de msg
                
                if(mensagem[0].equals("mensagem")){//msg local
                    verificouMsg = true;
                    mostrarMsg = msg.substring(9);
                    //se msg for do lider
                    if(cliente.getLider()){
                        enviaExterna.Envia(mostrarMsg);
                    }else{//msg interna
                        System.out.println("Recebeu msg: " +mostrarMsg);
                    }              
                }else if(mensagem[0].equals("MsgExt")){// msg externa
                    verificouMsg = true;
                    mostrarMsg = msg.substring(9);
                    System.out.println("Msg Externa: " +mostrarMsg);
                }
                //iniciar eleicao e já me propor como candidato
                else if(mensagem[0].equals("eleicao")){
                    verificouMsg = true;
                    multicastgroup.sendMessage("id:"+cliente.getID());
                    perdeuAntes = false;               
                }else if(mensagem[0].equals("id")){//Fazer escolha do lider
                    verificouMsg = true;
                    int candidato = Integer.parseInt(mensagem[1]);
                    
                    if(cliente.getID() == candidato){
                        System.out.println("Recebi meu proprio ID!");
                    }else if(cliente.getID() > candidato && perdeuAntes == false){//perdeuAntes
                         System.out.println("VIVA! GANHEI!");
                         cliente.setLider(true);
                              
                         try{
                            int defaultPort = 4444;
                            Socket conexao = new Socket("192.168.0.15", defaultPort);//239.0.0.1
                            
                            recebeExt = new ThreadRecebeMsgExterna(conexao.getInputStream(), cliente);
                            recebeExt.start();
                            
                            enviaExterna = new ThreadEnviaMsgExterna(conexao.getOutputStream(), recebeExt, cliente);
                         }catch(Exception e){
                            System.err.println(e);
                         }
                                   
                    }else{
                         cliente.setLider(false);
                         System.out.println("PERDI! -_- ");
                         perdeuAntes= true;
                    }             
                }else if(mensagem[0].equals("TesteLider")){//testa presença do lider
                    //se for o lider
                    if(cliente.getLider()){
                        System.out.println("Teste Realizado!");
                        multicastgroup.sendMessage("LIDERPRESENTE");
                    }else if(!verificouTdsP){
                        verificouMsg = true;
                        verificouTdsP = true;//Diz se tds o processos foram verficados se são líder
                    }
                
                }else if(mensagem[0].equals("RespostaLider")){//lider está presente
                    System.out.println("Lider Respondeu");
                    cliente.setEspera(false);
                    cliente.setContinua(true);
                    verificouTdsP = false;
                }
                
                if(!verificouMsg && cliente.getEspera() && !cliente.getContinua()){
                    System.out.println("Lider não responde...");
                    cliente.ComecarEleicao();
                
                }
                
                System.out.println("Msg: "+msg);
                
            }
            
        }catch(Exception e){
            System.err.println(e);
        }
        
    }
    
}
