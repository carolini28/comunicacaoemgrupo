/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemasdistribuidos;

import java.io.OutputStream;

/**
 *
 * @author Carolini
 */
class Saida {

    OutputStream outputStream;
    int i;
    
    Saida(OutputStream outputStream, int i) {
        this.outputStream = outputStream;
        this.i = i;
    }

    void gerar() {
        try{
            outputStream.write(i);
        }catch(Exception e){
            System.err.println(e);
        }
    }
    
}
