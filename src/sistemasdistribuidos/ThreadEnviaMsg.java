/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemasdistribuidos;

import javax.swing.JOptionPane;

/**
 *
 * @author Carolini
 */

// envia mensagem ao grupo 
public class ThreadEnviaMsg extends Thread{
    
    private MulticastGroup multicastgroup;
    Cliente cliente;
    
    ThreadEnviaMsg(MulticastGroup multicastgroup, Cliente cliente){
        this.multicastgroup = multicastgroup;
        this.cliente = cliente;
    }
    
    public void run(){
    
        try{
        
            while(true){
            
                String mensagem = JOptionPane.showInputDialog("ID: "+cliente.getID()+"\nInsira uma msg (ou digite: * )",null);
            
                if(mensagem.equals("*")){
                    break;
                }
                
                multicastgroup.sendMessage("mensagem: "+mensagem);
            }
            
        }catch(Exception e){
            System.err.println(e);
        }
   
        
    }
   
}
