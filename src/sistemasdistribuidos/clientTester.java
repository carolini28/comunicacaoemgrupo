/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemasdistribuidos;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Carolini
 */
public class clientTester {
    // Funcao Principal
    public static void main(String[] args) {
        int thePort=4444;
        ServerSocket ss;
        Socket theConnection;
        
        try {
           if(args.length == 1)
           thePort = Integer.parseInt(args[0]);
        } // end try
        catch (Exception e) {
           thePort = 0;
        } // end catch
        
        try {
            ss = new ServerSocket(thePort);
            System.out.println("Esperando por conexoes na porta: " + ss.getLocalPort());
            while (true) {
                theConnection = new Socket(ss.getInetAddress(), ss.getLocalPort());
                theConnection = ss.accept();
                System.out.println("Conexao estabelecida com " + theConnection);
               
                ThreadRecebeMsgExterna RmsgExt = new ThreadRecebeMsgExterna(theConnection.getInputStream(),null);
                RmsgExt.start();
                              
                ThreadEnviaMsgExterna EmsgExt = new ThreadEnviaMsgExterna(theConnection.getOutputStream(), RmsgExt, null);
                EmsgExt.start();
              
                
                try {
                    RmsgExt.join();
                    EmsgExt.join();
                } catch (Exception e) {
                    System.err.println(e);
                } // end catch
            } // end while
        } // end try
        catch (IOException e) {
            System.err.println(e);
        } // end catch
    }
}
