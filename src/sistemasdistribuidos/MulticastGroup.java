/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemasdistribuidos;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

/**
 *
 * @author Carolini
 */
public class MulticastGroup {
    
    private String mIP ="239.0.0.1"; // 224.XX.XX.XX - 239.XX.XX.XX -239.0.0.1
    private int mPort; // XXXX > 1024
    private byte mTTL=1;
    private MulticastSocket mSocket;
    private InetAddress mGroupAddrs=null;
    
    MulticastGroup (){
        this.mPort = 12345;
        if (mJoinGroup()){
            System.out.println("OK");
        }
    }
    
    public boolean mJoinGroup(){
        System.out.print("\nJoining group: ");
        try { 
            mGroupAddrs = InetAddress.getByName(mIP);
        }
        catch (UnknownHostException e){
            System.err.println("ERR: Can't resolve \""+mIP+"\": "+e);
            return false;
        }
        System.out.print(mGroupAddrs+":"+mPort+"... ");
        try {
            mSocket = new MulticastSocket(mPort);
        }
        catch (IOException e) {
            System.err.println("FAIL");
            System.err.println("Can't creat MulticasSocket:"+mPort+": "+ e);
            return false;
        }
        try {
            mSocket.joinGroup(mGroupAddrs);
        }
        catch (IOException e) {
              System.err.println("ERR: Can't join group "+mGroupAddrs+": "+ e);
              return false;
        }
        return true;
    }
    
    public boolean mLeaveGroup(){
        System.out.print("\nLeaving group...");
        try{ 
            mSocket.leaveGroup(mGroupAddrs); 
        }
        catch (IOException e) {
            System.out.println("ERR: Can't leave group: IO Exception:" + e);
            return false;
        }
        System.out.println("OK"); 
        return true;
    }
    
     //Envia uma mensagem para o grupo multicast
    public void sendMessage(String msg) throws IOException{
        byte[] data;
        try{
            
            msg = msg + "";
            data = msg.getBytes("UTF-8");
            DatagramPacket outgoing = new DatagramPacket(data, data.length, mGroupAddrs, mPort);
            mSocket.send(outgoing);
            System.out.println("Pacote Enviado: " + msg);
            
        }catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }
    
    //Recebe uma mensagem do grupo multicast
    public String receiveMessage() throws IOException {
         byte[] data = new byte[Cliente.bufferLength];
         try{
             DatagramPacket incoming = new DatagramPacket(data, data.length);
             mSocket.receive(incoming);
             return new String(incoming.getData(), "UTF-8");//MUDAR ISSO
         }catch(Exception ex){
             throw new RuntimeException(ex);
         }
    }
    
    public MulticastSocket getSocket(){
         return mSocket;
    }
    public int getPort(){
         return mPort;
    }
    public InetAddress getGroupAddrs(){
         return mGroupAddrs;
    }
    public void Close(){
         mSocket.close();
    }  
    
}
