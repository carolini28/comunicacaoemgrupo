/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemasdistribuidos;

import java.io.OutputStream;
import java.io.PrintStream;
import javax.swing.JOptionPane;

/**
 *
 * @author Carolini
 */
//responsavel pela comunicação externa ao grupo. Envia as mensagens
public class ThreadEnviaMsgExterna extends  Thread{

    ThreadRecebeMsgExterna recebeExt;
    Cliente cliente;
    PrintStream print;
            
    public ThreadEnviaMsgExterna(OutputStream out, ThreadRecebeMsgExterna recebeExt, Cliente cliente) {
        this.recebeExt = recebeExt;
        this.cliente = cliente;
        print = new PrintStream(out);
    }

    
    public void run(){
        try{
            String mostrar;
            String msg;
          
            System.out.println("OUTPUT THREAD");
            if(cliente != null){
                msg = "Lider: "+cliente.getID()+"msg ext:";
            }else{
                msg = "Msg Ext:";
            }
            
            while(true){
                mostrar = JOptionPane.showInputDialog(msg);
                print.print(mostrar + String.valueOf((char)32));
            }
        }catch(Exception e){
            System.err.println(e);
        }
       
         System.out.println("OUTPUT THREAD ENDING...");
         recebeExt.interrupt();
         this.interrupt();
         System.out.println("OUTPUT THREAD STOPPED");
    }
    
    
    void Envia(String mostrarMsg) {
        print.print(mostrarMsg + String.valueOf((char)32));
    }
    
}
