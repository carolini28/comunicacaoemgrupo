/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemasdistribuidos;

/**
 *
 * @author Carolini
 */
public class ThreadVerificaLider extends Thread{
    
    MulticastGroup multicastGroup;
    Cliente cliente;
    
    ThreadVerificaLider(MulticastGroup multicastGroup, Cliente cliente){
        this.multicastGroup = multicastGroup;
        this.cliente = cliente;
    }
    
    public void run(){
    
        try{
            while(true){
                if(!cliente.getLider()){              
                    cliente.setEspera(true);
                    cliente.setContinua(false);
                    //testar lider
                    multicastGroup.sendMessage("TesteLider");
                }
                
                //Thread.sleep(10000);
            }
        
        }catch(Exception e){
            System.err.println(e);
        
        }
    
    }
    
}
