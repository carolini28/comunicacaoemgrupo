/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemasdistribuidos;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carolini
 */
//Recebe mensagens externas, e envia ao grupo 
public class ThreadRecebeMsgExterna  extends  Thread{

    InputStream inputStream;
    Cliente cliente;
    MulticastGroup multicastgroup;
    
    public ThreadRecebeMsgExterna(InputStream inputStream, Cliente cliente) {
        this.inputStream = inputStream;
        this.cliente = cliente;
    }

    public void run() {
       try{ 
            boolean leitura = false;
            String mensagem = "";
            while(true){

                    int var = inputStream.read();

                    if(var == -1){//terminou a leitura
                        break;
                    }
                    
                    if(var != 0 && var != 32){//se for null ou espaço
                        mensagem +=String.valueOf((char) var);
                        leitura = true;
                    }
                    else if(leitura){
                        System.out.println("MsgExt:"+mensagem);
                        if(cliente != null){
                            multicastgroup.sendMessage("MsgExt:"+mensagem);
                        }
                        leitura = false;
                        mensagem = "";
                    }
            }
       }catch(Exception e){
           System.err.println(e);
       }
    
    }
    
}
