/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemasdistribuidos;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author Carolini
 */
public class GerarIDserverSocket {
    
    private static int PiD = 0;
    
    public static void main(String[] args){
    
         int defaultPort = 12345;
         ServerSocket ss;
         Socket conexao;
         
         try {
            if (args.length == 1) {
                defaultPort = Integer.parseInt(args[0]);
            }
        } catch (Exception e) {
            defaultPort = 0;
        }
         
        try{
            ss = new ServerSocket(defaultPort);
            defaultPort = ss.getLocalPort();
            
            while(true){
                conexao = ss.accept();
                Saida saida = new Saida(conexao.getOutputStream(), ++PiD);
                saida.gerar();
            }
        
        }catch(Exception e){
            System.err.println(e);
        }         
    }
    
    public static void setId(int Pid){
        PiD  = Pid;
    }
    
    public static int Gerar() throws UnknownHostException, IOException{
        int defaultPort = 51424;
        Socket conexao = new Socket("192.168.0.15", defaultPort);//239.0.0.1 192.168.0.15,port=51424
        GerarID gID = new GerarID(conexao.getInputStream());
        gID.Gerar();
        conexao.close();
        
        return PiD;
    }
    
    
    
}
