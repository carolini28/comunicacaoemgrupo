/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemasdistribuidos;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author Carolini
 */
public class GerarID {

  InputStream input;  
  public int pId;
  
  GerarID(InputStream input){
      this.input = input;
  }
    
  public void Gerar(){
        
        try{
            pId = input.read();
            GerarIDserverSocket.setId(pId);
        }catch(Exception e){
            System.err.println(e);
        }
    }
}

