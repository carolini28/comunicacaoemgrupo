/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemasdistribuidos;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author Carolini
 */
//Processo
public class Cliente extends Thread{
    
    public static int bufferLength = 8192;
    private int ID;
     
    public MulticastGroup multicastGroup;
    private ThreadEnviaMsg EnviaMsg;//saida de mensagens do grupo
    private ThreadRecebeMsg RecebeMsg;//entrada de mensagens no grupo
    private ThreadEnviaMsgExterna enviaExterna;
    private ThreadRecebeMsgExterna recebeExt;
    private ThreadVerificaLider verificaLider;
    private GerarID gerar;
    
    private boolean lider;
    private boolean espera = false;
    private boolean continua = false;
    
    public Cliente() throws UnknownHostException, IOException{
        multicastGroup = new MulticastGroup();
        EnviaMsg =  new ThreadEnviaMsg(multicastGroup,this);
        RecebeMsg = new ThreadRecebeMsg(this,multicastGroup);
        verificaLider = new ThreadVerificaLider(multicastGroup, this);
        
        espera = false;
        continua = false;
        
        //gerar id aki
        ID = GerarIDserverSocket.Gerar();
                
        if(ID == 1){//esse é o líder
           lider = true;
            try{
                int defaultPort = 4444;//239.0.0.1
                Socket conexao = new Socket("192.168.0.15", defaultPort);
                            
                recebeExt = new ThreadRecebeMsgExterna(conexao.getInputStream(), this);
                recebeExt.start();
                            
                enviaExterna = new ThreadEnviaMsgExterna(conexao.getOutputStream(), recebeExt, this);
             }catch(Exception e){
                System.err.println(e);
             }
        }else{
           lider = false;
        }
    
        System.out.println("Teste");
        
        EnviaMsg.start();
        RecebeMsg.start();
        verificaLider.start();
    }

       
    public int getID() {
        return ID;
    }
    
    public void setID(int pId) {
        this.ID = pId;
    }
    
    boolean getLider() {
        return lider;
    }
    
    public void setLider(boolean b) {     
            this.lider = b;
    }

    public boolean getEspera() {
        return espera;
    }

    public void setEspera(boolean espera) {
        this.espera = espera;
    }

    public boolean getContinua() {
        return continua;
    }

    public void setContinua(boolean continua) {
        this.continua = continua;
    }

    void ComecarEleicao() throws IOException {
        multicastGroup.sendMessage("eleicao");
    }
    
    public static void main(String[] args) throws UnknownHostException, IOException{   
        Cliente c = new Cliente();
        c.start();
    }

     
   
}
